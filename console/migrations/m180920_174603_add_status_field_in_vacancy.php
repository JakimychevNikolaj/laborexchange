<?php

use yii\db\Migration;

/**
 * Class m180920_174603_add_status_field_in_vacancy
 */
class m180920_174603_add_status_field_in_vacancy extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('vacancy', 'status', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('vacancy', 'status');
    }
}
