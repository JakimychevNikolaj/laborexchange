<?php

use yii\db\Migration;

/**
 * Class m180919_181240_add_moderator
 */
class m180919_181240_add_moderator extends Migration
{
	public function safeUp()
	{
		$email = 'moderator@mail.com';
		$this->insert('user', array(
			'email' =>	$email,
			'passwordHash' => \Yii::$app->security->generatePasswordHash('123456'),
			'name' => stristr($email, '@', true),
			'role' => \Yii::$app->params['roleModerator'],
			'createdAt' => time(),
			'updatedAt' => time()
		));
	}
}
