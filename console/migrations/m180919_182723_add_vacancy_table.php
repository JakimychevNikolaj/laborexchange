<?php

use yii\db\Migration;

/**
 * Class m180919_182723_add_vacancy_table
 */
class m180919_182723_add_vacancy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('vacancy', [
			'vacancyId' => $this->primaryKey(),
			'userId' => $this->integer(),
			'title' => $this->string()->notNull(),
			'description' => $this->text(),
			'email' => $this->string(),
			'createdAt' => $this->bigInteger(),
			'updatedAt' => $this->bigInteger()
		]);
		$this->addForeignKey(
			'fk-vacancy-user-1',
			'vacancy',
			'userId',
			'user',
			'userId',
			'SET NULL'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('fk-vacancy-user-1', 'vacancy');
    	$this->dropTable('vacancy');
    }
}
