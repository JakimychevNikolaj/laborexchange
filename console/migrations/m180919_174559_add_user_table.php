<?php

use yii\db\Migration;

/**
 * Class m180919_174559_add_user_table
 */
class m180919_174559_add_user_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
        $this->createTable('user', [
            'userId' => $this->primaryKey(),
        	'name' => $this->string(),
        	'passwordHash' => $this->string(),
        	'role' => $this->smallInteger()->notNull()->defaultValue(\Yii::$app->params['roleEmployer']),
            'email' => $this->string()->notNull()->unique(),
        	'vacancyWasPublished' => $this->boolean()->defaultValue(false),
        	'createdAt' => $this->bigInteger(),
        	'updatedAt' => $this->bigInteger()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
