<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\db\User */
?>
    <p>Здравствуйте <?= Html::encode($user->name) ?>,</p>

    <p>Вакансия, которую вы разместили находится на этапе модерации.</p>
