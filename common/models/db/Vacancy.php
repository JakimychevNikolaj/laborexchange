<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vacancy".
 *
 * @property int $vacancyId
 * @property int $userId
 * @property string $title
 * @property string $description
 * @property string $email
 * @property int $createdAt
 * @property int $updatedAt
 * @property int $status
 *
 * @property User $user
 */
class Vacancy extends \yii\db\ActiveRecord
{
	const STATUS_DECLINED = -1;
	const STATUS_NOT_MODERATED = 0;
	const STATUS_MODERATED = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'createdAt', 'updatedAt', 'status'], 'integer'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['title', 'email'], 'string', 'max' => 255],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'userId']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
			'vacancyId' => 'ID',
			'userId' => 'Пользователь',
			'title' => 'Заголовок',
			'description' => 'Описание',
			'email' => 'Почтовый адрес',
			'createdAt' => 'Дата создания',
			'updatedAt' => 'Дата обновления',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['userId' => 'userId']);
    }
}
