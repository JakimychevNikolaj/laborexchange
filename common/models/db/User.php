<?php

namespace common\models\db;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $userId
 * @property string $name
 * @property string $passwordHash
 * @property int $role
 * @property string $email
 * @property int $vacancyWasPublished
 * @property int $createdAt
 * @property int $updatedAt
 *
 * @property Vacancy[] $vacancies
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
	const ROLE_EMPLOYER = 0;
	const ROLE_MODERATOR = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role', 'vacancyWasPublished', 'createdAt', 'updatedAt'], 'integer'],
            [['email'], 'required'],
            [['name', 'passwordHash', 'email'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'ID',
            'name' => 'Имя',
            'passwordHash' => 'Пароль',
            'role' => 'Роль',
            'email' => 'Почтовый адрес',
            'vacancyWasPublished' => 'Вакансия была опубликована',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата обновления',
        ];
    }

	// 5 методов для IdentityInterface:
	// - findIdentity($id)
	// - findIdentityByAccessToken($token, $type = null)
	// - getId()
	// - getAuthKey()
	// - validateAuthKey($authKey)
    public function setPassword($password)
	{
		$this->passwordHash = \Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::findOne(['userId' => $id]);
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return null;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->passwordHash);
	}

	/**
	 * @return $this|null
	 */
	public static function getUser()
	{
		return (!empty(Yii::$app->user)) ? Yii::$app->user->getIdentity() : null;
	}

	public function isModerator()
	{
		return $this->role == static::ROLE_MODERATOR;
	}

	public function isEmployer()
	{
		return $this->role == static::ROLE_EMPLOYER;
	}


	/**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancies()
    {
        return $this->hasMany(Vacancy::className(), ['userId' => 'userId']);
    }
}
