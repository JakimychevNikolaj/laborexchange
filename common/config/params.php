<?php
return [
	'supportEmail' => 'support@example.com',
	'moderatorEmail' => 'moderator@example.com',
	'roleEmployer' => 0,
	'roleModerator' => 1
];
