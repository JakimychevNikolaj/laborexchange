<?php

use common\models\db\User;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\db\Vacancy;

/* @var $this yii\web\View */
/* @var $model common\models\db\Vacancy */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Vacancies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$user = User::getUser();
?>
<div class="vacancy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php if($user->isEmployer() && $model->userId == $user->userId):?>
			<?= Html::a('Update', ['update', 'id' => $model->vacancyId], ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Delete', ['delete', 'id' => $model->vacancyId], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method' => 'post',
				],
			]) ?>
		<?php elseif ($user->isModerator()):?>
			<?= Html::a('Промодерировать', ['change-status', 'id' => $model->vacancyId, 'status' => Vacancy::STATUS_MODERATED], ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Отказать в модерации', ['change-status', 'id' => $model->vacancyId, 'status' => Vacancy::STATUS_DECLINED], [
				'class' => 'btn btn-danger',
			]) ?>
        <?php endif?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vacancyId',
            'userId',
            'title',
            'description:ntext',
            'email:email',
        ],
    ]) ?>

</div>
