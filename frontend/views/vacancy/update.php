<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\db\Vacancy */

$this->title = 'Update Vacancy: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Vacancies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->vacancyId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacancy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
