<?php

use common\models\db\User;
use common\models\db\Vacancy;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\VacancySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vacancies';
$this->params['breadcrumbs'][] = $this->title;
$user = User::getUser();
$actionColumn = ($user->role)
    ?
	['class' => 'yii\grid\ActionColumn',
		'template' =>'{view} {accept} {decline}',
		'buttons' => [
			'view' => function ($url, $model) {
				return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
					['vacancy/view', 'id' => $model->vacancyId],
					['title' => 'Просмотреть']);
			},
			'accept' => function ($url, $model) {
				return Html::a('<span class="glyphicon glyphicon-ok"></span>',
					['vacancy/change-status', 'id' => $model->vacancyId, 'status' => Vacancy::STATUS_MODERATED],
					[
						'title' => 'Промодерировать',
					]);
			},
			'decline' => function ($url, $model) {
				return Html::a('<span class="glyphicon glyphicon-remove"></span>',
					['vacancy/change-status', 'id' => $model->vacancyId, 'status' => Vacancy::STATUS_DECLINED],
					[
						'title' => 'Отказать в модерации',
					]);
			},
		]
	]
    :
	[
		'class' => 'yii\grid\ActionColumn',
		'visibleButtons' => [
			'view' => function ($model, $key, $index) use ($user) {
				return $model->userId === $user->userId;
			},
			'update' => function ($model, $key, $index) use ($user) {
				return $model->userId === $user->userId;
			},
			'delete' => function ($model, $key, $index) use ($user) {
				return $model->userId === $user->userId;
			},
		]
	]
?>
<div class="vacancy-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if($user->isEmployer()):?>
			<?= Html::a('Создать вакансию', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vacancyId',
            'userId',
            'title',
            'description:ntext',
            'email:email',
            //'createdAt',
            //'updatedAt',

            $actionColumn,
        ],
    ]); ?>
</div>
