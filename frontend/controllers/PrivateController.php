<?php

namespace frontend\controllers;

class PrivateController extends \yii\web\Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					// deny all not authenticated users
					[
						'allow' => false,
						'roles' => ['?'],
					],
					// allow authenticated users
					[
						'allow' => true,
					],
				],
			],
		];
	}
}
