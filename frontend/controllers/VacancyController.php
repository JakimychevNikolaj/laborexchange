<?php

namespace frontend\controllers;

use common\models\db\User;
use Yii;
use common\models\db\Vacancy;
use frontend\models\VacancySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class VacancyController extends PrivateController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
		$behaviors = parent::behaviors();

		$behaviors['verbs'] = [
			'class' => VerbFilter::className(),
			'actions' => [
				'list' => ['get']
			]
		];

		return $behaviors;
    }

    /**
     * Lists all Vacancy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VacancySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vacancy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vacancy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vacancy();
        $user = User::getUser();
        $model->userId = $user ? $user->userId : null;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	if ($user->isEmployer() && ($user->vacancyWasPublished == 0)) {
        		$user->vacancyWasPublished = 1;
        		$user->save();
				$sendingStatus = Yii::$app
					->mailer
					->compose(
						['html' => 'notice-html', 'text' => 'notice-text'],
						['user' => $user]
					)
					->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
					->setTo($user->email)
					->setSubject('Статус вакансии')
					->send();
				Yii::trace("sending status to employer: $sendingStatus");

				$sendingStatus = Yii::$app
					->mailer
					->compose(
						['html' => 'mnotice-html', 'text' => 'mnotice-text']
					)
					->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
					->setTo(Yii::$app->params['moderatorEmail'])
					->setSubject('Появились новые вакансии')
					->send();
				Yii::trace("sending status to moderator: $sendingStatus");

			}
            return $this->redirect(['view', 'id' => $model->vacancyId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionChangeStatus($id, $status)
	{
		$user = User::getUser();
		$vacancy = Vacancy::findOne($id);
		if ($user && ($user->isModerator()) && $vacancy) {
			$vacancy->status = $status;
			$vacancy->save();
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

    /**
     * Updates an existing Vacancy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$user = User::getUser();
		$model->userId = $user ? $user->userId : null;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vacancyId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vacancy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vacancy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vacancy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vacancy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
