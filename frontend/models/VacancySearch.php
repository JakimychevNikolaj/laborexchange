<?php

namespace frontend\models;

use common\models\db\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\db\Vacancy;

/**
 * VacancySearch represents the model behind the search form of `common\models\db\Vacancy`.
 */
class VacancySearch extends Vacancy
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vacancyId', 'userId', 'createdAt', 'updatedAt'], 'integer'],
            [['title', 'description', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacancy::find();
        $user = User::getUser();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($user->isModerator()) {
			$query->andWhere(['status' => Vacancy::STATUS_NOT_MODERATED]);
		}

        // grid filtering conditions
        $query->andFilterWhere([
            'vacancyId' => $this->vacancyId,
            'userId' => $this->userId,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
